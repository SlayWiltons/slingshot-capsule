using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShowMessage : MonoBehaviour
{
    [SerializeField] private GameObject info;
    [SerializeField] private TMP_Text infoText;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Finish")
        {
            Time.timeScale = 0;
            infoText.color = Color.green;
            infoText.text = "Congrats!!!";
            info.SetActive(true);
        }
        else if (collision.gameObject.tag == "DeadZone")
        {
            Time.timeScale = 0;
            infoText.color = Color.red;
            infoText.text = "Fail";
            info.SetActive(true);
        }
    }    
}
