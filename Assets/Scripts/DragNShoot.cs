using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class DragNShoot : MonoBehaviour
{
    private Vector3 mousePressDownPos;
    private Vector3 mouseReleasePos;

    private Rigidbody rb;

    public bool isOnGround;

    Touch touch;


    public float forceMultiplier = 3;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }    

    private void Shoot (Vector3 force)
    {
        rb.AddForce(new Vector3(force.x / 2, force.y, force.y) * forceMultiplier);        
    }

    private void Update()
    {
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                mousePressDownPos = Input.mousePosition;
            }


            if (touch.phase == TouchPhase.Moved)
            {
                Vector3 forceInit = (Input.mousePosition - mousePressDownPos);
                Vector3 forceV = (new Vector3(forceInit.x / 2, forceInit.y, forceInit.y)) * forceMultiplier;
                if (Input.mousePosition.y < mousePressDownPos.y)
                {
                    DrawTrajectory.Instance.UpdateTrajectory(forceV, rb, transform.position);
                }
                else
                {
                    DrawTrajectory.Instance.HideLine();
                }
            }

            if (touch.phase == TouchPhase.Ended)
            {
                if (Input.mousePosition.y < mousePressDownPos.y)
                {
                    DrawTrajectory.Instance.HideLine();
                    mouseReleasePos = Input.mousePosition;
                    if (isOnGround == true)
                    {
                        Shoot(mousePressDownPos - mouseReleasePos);
                    }
                }
            }
        }
    }
}