using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonRestart : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject startPoint;
    [SerializeField] private GameObject info;

    private Rigidbody playerRb;

    private void Start()
    {
        playerRb = player.GetComponent<Rigidbody>();
    }

    public void Restart()
    {
        info.SetActive(false);
        SceneManager.LoadScene(0);
    }
}
