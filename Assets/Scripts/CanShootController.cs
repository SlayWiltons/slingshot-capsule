using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanShootController : MonoBehaviour
{
    public bool isShoot = true;

    [SerializeField] private GameObject player;

    private void OnCollisionEnter(Collision collision)
    {
        collision.gameObject.tag = "Player";
        player.GetComponent<DragNShoot>().isOnGround = true;
    }

    private void OnCollisionExit(Collision collision)
    {
        collision.gameObject.tag = "Player";
        player.GetComponent<DragNShoot>().isOnGround = false;
    }
}
